import sqlite3
from sqlite3 import Connection, Cursor


class DBConnection:
    def __init__(self, connection: Connection):
        self.connection = connection
        self.cursor: Cursor = None

    def __enter__(self):  # running after 'with'
        self.cursor = self.connection.cursor()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if isinstance(exc_val, Exception):
            self.connection.rollback()
        else:
            self.connection.commit()
        self.connection.close()
