from sqlite3 import Cursor


def create_table_sql(table_name: str) -> str:
    return f'''
        CREATE TABLE {table_name} (
            id integer primary key autoincrement,
            email text not null ,
            name text not null ,
            item text not null ,
            return_date date
        )
    '''

def get_user_by_date() -> str:
    return '''SELECT name, email, item, return_date FROM borows WHERE return_date < ?'''