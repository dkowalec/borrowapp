import sqlite3
from datetime import date

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from os import getenv, path

from dotenv import load_dotenv
from jinja2 import Environment, FileSystemLoader, select_autoescape
from borowers import get_borrowers_by_return_date
from send_mail import EmailSender, Credentials

load_dotenv()

env = Environment(
    loader=FileSystemLoader('%s/templates/' % path.dirname(__file__)),
    autoescape=select_autoescape()
)

connection = sqlite3.connect(getenv('DB_NAME'))


sender = f"Dariusz Kowalec <{getenv('EMAIL_FROM')}>"
receiver = getenv('EMAIL_TO')
password = getenv('PASSWORD')


def send_mail_to_borrower(borrower):
    message = MIMEMultipart()
    message['FROM'] = sender
    message['TO'] = receiver  # email
    message['Subject'] = 'Prośba o zwrot'
    output = template.render(name=borrower.name, item=borrower.item, return_date=borrower.return_date)
    message.attach(MIMEText(output, 'html'))

    connection.sendmail(sender, receiver, message)

if __name__ == '__main__':
    template = env.get_template("mail.html")
    credentials = Credentials(getenv('EMAIL_FROM'), password)
    current_date = date.today().strftime('%Y-%m-%d')
    borrowers = get_borrowers_by_return_date(connection, current_date)
    with EmailSender(port=465, smtp_server='smtp.gmail.com', credentials=credentials) as connection:
        for borrower in borrowers:
           send_mail_to_borrower(borrower)
