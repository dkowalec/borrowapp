from collections import namedtuple
from sqlite3 import Connection

from db_manager import DBConnection
from queries import get_user_by_date

User = namedtuple('User', 'name, email, item, return_date')


def get_all(connection: Connection):
    with DBConnection(connection) as db:
        db.cursor = db.connection.execute('SELECT * FROM borows')
        data = []
        for id, email, name, item, return_date in db.cursor.fetchall():
            data.append(User(name,email,item,return_date))
        return data


def get_borrowers_by_return_date(connection: Connection, date: str):
    with DBConnection(connection) as db:
        db.cursor.execute(get_user_by_date(), (date,))
        entities = []
        for name, email, item, return_date in db.cursor.fetchall():
            entities.append(User(name,email,item,return_date))
        return entities