import sqlite3
from sqlite3 import Connection


def connection() -> Connection:
    connection = sqlite3.connect('database.db')
    return connection
