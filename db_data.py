sample_data = [
    (1,'magdalena@test.email', 'Magda', 'Hyłka', '2024-01-02'),
    (2,'paweł@test.email', 'Paweł', 'Siekiera', '2021-12-12'),
    (3,'magdalena@test.email', 'Magda', 'Potop', '2019-11-12'),
    (4,'andrzej@test.email', 'Andrzej', 'Ostry cień mgły', '2022-01-01'),
    (5,'adam@test.email', 'Adam', 'Scyzoryk', '2023-12-31'),
    (6,'janek@test.email', 'Jane', 'Pan Tadeusz', '2023-12-12')
]