import sqlite3
from datetime import date
from sqlite3 import Connection

import pytest
from db_data import sample_data
from borowers import get_borrowers_by_return_date


@pytest.fixture
def create_connection() -> Connection:
    connection = sqlite3.connect(":memory:")
    cursor = connection.cursor()
    cursor.execute('''
        CREATE TABLE borows (
            id integer primary key autoincrement,
            email text not null ,
            name text not null ,
            item text not null ,
            return_date date
        )''')

    cursor.executemany('INSERT INTO borows VALUES(?,?,?,?,?)', sample_data)

    return connection


def test_borrowers(create_connection: Connection):
    current_date = date.today().strftime('%Y-%m-%d')
    borrowers = get_borrowers_by_return_date(create_connection, current_date)

    assert len(borrowers) == 3
    assert borrowers[0].name == 'Paweł'
    assert borrowers[1].name == 'Magda'
